from sys import stderr

class Logger:
    '''
    Utility class used to print messages (information or warnings).
    '''

    def __init__(self, context: str, verbose: bool):
        self.context = context
        self.verbose = verbose

    def log(self, text: str) -> None:
        if self.verbose:
            print('%s: %s' % (self.context, text) )

    def warn(self, text: str) -> None:
        stderr.write('warning %s: %s\n' % (self.context, text))
