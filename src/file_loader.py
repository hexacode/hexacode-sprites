from sys import exit as sys_exit
from typing import List, TextIO, Dict

import yaml

from tile import Tile


def get_tiles(tiles_file: TextIO) -> List[Tile]:
    print('Loading tiles file...')

    tiles = []
    with tiles_file:
        try:
            tiles_dict = yaml.safe_load(tiles_file)
            for tile_dict in tiles_dict:
                tiles.append(Tile.from_dict(tile_dict))
        except yaml.YAMLError:
            print('Can not parse tiles file `%s`.' % tiles_file.name)
            sys_exit(1)
    return tiles

def get_sprite_style(sprite_style_file: TextIO) -> Dict[str, str]:
    # TODO: use CSS file instead
    print('Getting sprite style...')

    with sprite_style_file:
        try:
            sprite_style = yaml.safe_load(sprite_style_file)
            try:
                return sprite_style['colors']
            except KeyError:
                print('Sprites style file does not include color section.')
        except yaml.YAMLError:
            print('Can not parse sprite style file %s.' % sprite_style_file.name)
    sys_exit(1)
