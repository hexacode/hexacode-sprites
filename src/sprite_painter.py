from typing import Dict
from argparse import Namespace as Args
from enum import Enum

import xml.etree.ElementTree as ET

from logger import Logger
from tile import Tile


VIRTUAL_WIDTH = 512
ICON_SCALE = 3
PATH_WIDTH = 30


class Direction(Enum):
    '''Enumeration of directions corresponding to hexagon edges.'''

    WEST = 0
    NORTH_WEST = 60
    SOUTH_WEST = -60
    NORTH_EAST = 120
    SOUTH_EAST = -120
    EAST = 180


class SpritePainter():
    '''
    Draw the sprite of given tile.
    '''

    def __init__(self, tile: Tile, args: Args, colors: Dict[str, str]):
        self.tile = tile
        self.args = args
        self.colors = colors
        self.root = self.build_root()
        self.logger = Logger('tile %s' % tile.label, args.verbose)

    def build_root(self) -> ET.Element:
        root = ET.Element('svg')
        root.set('width', str(self.args.width))
        root.set('height', str(self.args.width))
        root.set('viewBox', '0 0 %d %s' % (VIRTUAL_WIDTH, VIRTUAL_WIDTH) )
        root.set('style', 'stroke-width:5;stroke-linecap:round;stroke-linejoin:round')
        return root

    def draw_tile(self) -> ET.Element:
        if len(self.tile.slots) == 1:
            self.draw_tile_constant()
        elif len(self.tile.slots) == 6:
            self.draw_tile_function()
        else:
            self.logger.warn('length of slots identifier must be 1 or 6, ' \
                + 'but was %d (`%s`), skipping tile' % ( len(self.tile.slots), self.tile.slots ))
        return self.root

    def draw_tile_constant(self) -> None:
        self.logger.log('drawing constant sprite')

        try:
            color = self.colors[self.tile.slots[0]]
        except KeyError:
            self.logger.warn('unrecognised constant type `%s`, skipping tile' % self.tile.slots[0])
            return

        circle = ET.SubElement(self.root, 'circle')
        circle.set('style', 'fill:%s;stroke:none' % color)
        circle.set('r', '60.472')
        circle.set('cx', '%d' % (VIRTUAL_WIDTH / 2) )
        circle.set('cy', '%d' % (VIRTUAL_WIDTH / 2) )

        self.draw_icon()

        self.logger.log('drawing finished.')

    def draw_tile_function(self) -> None:
        self.logger.log('drawing function sprite')

        self.draw_center()
        for dir_id, char in enumerate(self.tile.slots):
            self.draw_slot(list(Direction)[dir_id], char)
        self.draw_icon()

        self.logger.log('drawing finished')

    def draw_slot(self, direction: Direction, char: str) -> None:
        try:
            color_id = self.colors[char.lower()]
        except KeyError:
            self.logger.warn('unrecognised type `%s` in slots id `%s`' % (char, self.tile.slots))
            return

        if char == '_':
            self.draw_unused(direction)
        elif char.islower():
            self.draw_input(direction, color_id)
        else:
            self.draw_output(direction, color_id)

    def get_empty_slot(self, direction: Direction) -> ET.Element:
        slot = ET.SubElement(self.root, 'g')
        slot.set('transform', 'rotate(%d, %d, %d)' % (direction.value, 256, 256))
        return slot

    def draw_input(self, direction: Direction, color: str) -> None:
        self.logger.log('drawing input slot on %s with color %s' % (color, direction.name))
        slot = self.get_empty_slot(direction)

        data_type = ET.SubElement(slot, 'path')
        data_type.set('style', 'fill:%s;stroke:none' % color)
        data_type.set('d', 'm 82.794915,355.99855 v -39.7635 c 33.397765,0.23622 60.472005,-26.83802 60.472005,-60.23579 0,-33.39776 -27.07424,-60.47199 -60.472005,-60.23577 v -39.76347 l 43.464255,24.71695 m 0,0 a 86.9285,86.9285 0 0 1 43.46425,75.28229 86.9285,86.9285 0 0 1 -43.46425,75.28229 l -43.464255,24.717')

        contour = ET.SubElement(slot, 'path')
        contour.set('style', 'fill:none;stroke:%s' % self.colors['stroke'])
        contour.set('d', 'm 82.794915,355.99854 v -39.7635 c 33.397765,0.23622 60.472005,-26.83802 60.472005,-60.23578 0,-33.39777 -27.07424,-60.47199 -60.472005,-60.23577 v -39.76347')

    def draw_output(self, direction: Direction, color: str) -> None:
        self.logger.log('drawing output slot on %s with color %s' % (color, direction.name))
        slot = self.get_empty_slot(direction)

        background = ET.SubElement(slot, 'path')
        background.set('style', 'fill:%s;stroke:none' % self.colors['background'])
        background.set('d', 'm 82.794916,355.76201 c 0,-40.37568 0,-170.18747 0,-199.99852 l 43.464264,24.71695 m 0,0 c 26.89577,15.52828 43.46425,44.22573 43.46425,75.28229 0,31.05656 -16.56848,59.754 -43.46425,75.28228 l -43.464264,24.717')

        circle = ET.SubElement(slot, 'circle')
        circle.set('style', 'fill:%s;stroke:none' % color)
        circle.set('r', '60.472')
        circle.set('cx', '82.794914')
        circle.set('cy', '256')

        contour = ET.SubElement(slot, 'path')
        contour.set('style', 'fill:none;stroke:%s' % self.colors['stroke'])
        contour.set('d', 'm 82.794915,355.99854 v -39.7635 c -21.604563,0.23622 -41.568002,-11.28969 -52.370285,-29.99978 -10.802283,-18.7101 -10.802283,-41.7619 0,-60.472 10.802283,-18.7101 30.765722,-30.236 52.370285,-29.99977 v -39.76347')

    def draw_unused(self, direction: Direction) -> None:
        self.logger.log('drawing unused slot on %s' % direction.name)
        slot = self.get_empty_slot(direction)

        background = ET.SubElement(slot, 'path')
        background.set('style', 'fill:%s;stroke:none' % self.colors['background'])
        background.set('d', 'm 82.794915,156.00002 86.601915,49.99963 v 99.99926 l -86.601915,49.99963 z')

        contour = ET.SubElement(slot, 'path')
        contour.set('style', 'fill:none;stroke:%s' % self.colors['stroke'])
        contour.set('d', 'M 82.794915,156.00002 V 355.99854')

    def draw_icon(self) -> None:
        self.logger.log('drawing icon')

        offset = VIRTUAL_WIDTH / 2 - PATH_WIDTH / 2 * ICON_SCALE
        icon_group = ET.SubElement(self.root, 'g')
        icon_group.set('transform', 'translate(%d, %d) scale(%.2f, %.2f)' \
            % (offset, offset, ICON_SCALE, ICON_SCALE))

        icon = ET.SubElement(icon_group, 'path')
        icon.set('style', 'fill:none;stroke:%s' % self.colors['stroke'])
        icon.set('d', self.tile.icon)

    def draw_center(self) -> None:
        self.logger.log('drawing center')

        background = ET.SubElement(self.root, 'path')
        background.set('style', 'fill:%s;stroke:none' % self.colors['background'])
        background.set('d', 'm 82.796185,156.00073 17.320375,-9.99993 69.28152,39.99971 69.28152,-39.99971 V 66.00139 l 17.32039,-9.99992 17.32038,9.99992 v 79.99941 l 69.28153,39.99971 69.28152,-39.99971 17.3204,9.99993 v 19.99985 l -69.28154,39.99971 v 79.99941 l 69.28154,39.99971 v 19.99985 l -17.3204,9.99993 -69.28152,-39.99971 -69.28153,39.99971 v 79.99942 l -17.32038,9.99992 -17.32039,-9.99992 v -79.99942 l -69.28152,-39.99971 -69.28152,39.99971 -17.320375,-9.99993 V 335.99941 L 152.0777,295.9997 V 216.00029 L 82.796185,176.00058 Z')
