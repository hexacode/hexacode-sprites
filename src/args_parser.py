import os.path as op
from sys import argv
import argparse
from enum import Enum


PROJECT_PATH = op.dirname(op.dirname(op.abspath(__file__)))
DEFAULT_STYLE_PATH = op.join(PROJECT_PATH, 'style.yml')
DEFAULT_TYLES_PATH = op.join(PROJECT_PATH, 'tiles.yml')
DEFAULT_OUTPUT_DIR = op.join(PROJECT_PATH, 'sprites')


class Format(Enum):
    '''Enumeration of available file formats.'''

    SVG = 'svg'
    PNG = 'png'
    ATLAS = 'atlas'


def parse_args() -> argparse.Namespace:
    formatter = lambda prog: argparse.RawTextHelpFormatter(prog, max_help_position=10)

    parser = argparse.ArgumentParser(
        description='Hexacode Sprites - Generate hexacode sprites.',
        usage='%s [options] [-t tiles label] [tiles_file]\n' % argv[0] \
            + 'example: %s plus minus 1 2 3' % argv[0],
        formatter_class=formatter)

    parser.add_argument('tiles_file', nargs='?', type=open, default=DEFAULT_TYLES_PATH,
                        help='path of yaml file listing tiles from which to draw sprites')

    parser.add_argument('-s', '--style-path', type=open, default=DEFAULT_STYLE_PATH,
                        help='path of yaml file containing sprites style')

    parser.add_argument('-f', '--format', type=Format, choices=list(Format), default=Format.ATLAS,
                        help='file format - one of: [png, svg, atlas (default)]')

    parser.add_argument('-w', '--width', type=int, default=512,
                        help='size of generated sprites')

    parser.add_argument('-o', '--output-dir', default=DEFAULT_OUTPUT_DIR,
                        help='path of directory where generated sprites will be stored')

    parser.add_argument('-v', '--verbose', action='store_true',
                        help='enable verbose mode')

    return parser.parse_args()
