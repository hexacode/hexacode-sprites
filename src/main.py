#! .venv/bin/python

from typing import Dict
from io import BytesIO
import os.path as op

from args_parser import parse_args, Format
from sprite_painter import SpritePainter
from sprite_exporter import SpriteExporter
from file_loader import get_tiles, get_sprite_style


if __name__ == '__main__':
    args = parse_args()
    colors = get_sprite_style(args.style_path)

    sprites: Dict[str, BytesIO] = {}
    for tile in get_tiles(args.tiles_file):
        sprite_painter = SpritePainter(tile, args, colors)
        svg_tile = sprite_painter.draw_tile()

        exporter = SpriteExporter(svg_tile, args, tile)

        if args.format == Format.SVG:
            exporter.export_svg()
        elif args.format == Format.PNG:
            exporter.export_png()
        else:
            sprites['%d_%s' % (tile.marker, tile.label.replace(' ', '-'))] = exporter.get_png()

    if args.format == Format.ATLAS:
        atlas_img_path = op.abspath(op.join(args.output_dir, 'atlas.png'))
        atlas_index_path = op.abspath(op.join(args.output_dir, 'atlas.txt'))
        SpriteExporter.export_atlas(sprites, args.width, atlas_img_path, atlas_index_path)
