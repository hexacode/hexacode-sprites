from typing import Any, Dict


class Tile:
    '''
    Model class that represents a tile.
    '''

    def __init__(self, label: str, slots: str, icon: str, marker: int):
        self.label = label
        self.slots = slots
        self.icon = icon
        self.marker = marker

    @classmethod
    def from_dict(cls, dtile: Dict[str, Any]):
        try:
            return cls(dtile['label'], dtile['slots'], dtile['icon'], dtile['marker'])
        except KeyError as err:
            raise KeyError('Requires keys [label, slots, icon, marker], but was [%s].' % \
                ', '.join( dtile.keys() )) from err
