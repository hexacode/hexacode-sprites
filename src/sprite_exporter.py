from argparse import Namespace as Args
import os
from os import path as op
from io import BytesIO
from typing import Dict

import xml.etree.ElementTree as ET
from cairosvg import svg2png
from PIL import Image

from logger import Logger
from tile import Tile


class SpriteExporter:
    '''
    Export sprites to files in various formats.
    '''

    MAX_ATLAS_WIDTH = 230

    def __init__(self, svg_root: ET.Element, args: Args, tile: Tile):
        self.args = args
        self.tile = tile

        self.logger = Logger('tile %s' % tile.label, args.verbose)
        self.svg = self.build_svg(svg_root)

        if not op.exists(self.args.output_dir):
            self.logger.log('creating output directory at %s...' % self.args.output_dir)
            os.makedirs(self.args.output_dir)

    def build_svg(self, svg_root: ET.Element) -> str:
        self.logger.log('creating svg image')

        try:
            ET.indent(svg_root, space='\t')
        except AttributeError:
            self.logger.warn('can not indent file (require Python 3.9)')

        svg = ET.tostring(svg_root, encoding='utf-8', xml_declaration=True)
        self.logger.log('svg created')
        return svg

    def export_svg(self) -> None:
        self.logger.log('saving svg image')

        file_name = '%d_%s.svg' % (self.tile.marker, self.tile.label)
        svg_path = op.abspath(op.join(self.args.output_dir, file_name))
        with open(svg_path, 'w', encoding='utf-8') as svg_file:
            svg_file.write(self.svg)

        self.logger.log('svg file saved at `%s`' % svg_path)

    def export_png(self) -> None:
        self.logger.log('building and saving png image')

        file_name = '%d_%s.png' % (self.tile.marker, self.tile.label)
        file_path = op.abspath(op.join(self.args.output_dir, file_name))
        svg2png(bytestring=self.svg, write_to=file_path)

        self.logger.log('png file created and saved at `%s`.\n' % file_path)

    def get_png(self) -> BytesIO:
        self.logger.log('building png image for spritesheet')

        png_data = BytesIO()
        svg2png(bytestring=self.svg, write_to=png_data)

        self.logger.log('png file created.\n')
        return png_data

    @classmethod
    def export_atlas(cls, sprites: Dict[str, BytesIO], sprite_width: int,
            atlas_img_path: str, atlas_index_path: str) -> None:
        nb_cols = cls.MAX_ATLAS_WIDTH // sprite_width
        nb_rows = len(sprites) // nb_cols + 1

        img_size = sprite_width * nb_cols, sprite_width * nb_rows
        mosaic = Image.new('RGBA', img_size)

        with open(atlas_index_path, 'w', encoding='utf-8') as atlas_index_file:
            atlas_index_file.write('#0;0\t%dx%d\n' % (sprite_width, sprite_width) )
            for sprite_id, (sprite_name, sprite_bytes) in enumerate(sprites.items()):
                sprite = Image.open(sprite_bytes)
                pos_x = sprite_width * (sprite_id % nb_cols )
                pos_y = sprite_width * (sprite_id // nb_cols )
                mosaic.paste(sprite, (pos_x, pos_y) )
                atlas_index_file.write('%d;%d\t%s\n' % (pos_x, pos_y, sprite_name) )

        mosaic.save(atlas_img_path)
