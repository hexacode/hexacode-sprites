# Hexacode sprites

The **Hexacode** project is a visual programming language based on hexagons.

This **hexacode-sprites** repository is dedicated to generate and store hexacode sprites, that can be used to create *constant* and *function* tiles in the various Hexacode projects.


## Usage

```
./build_sprites.py [options] <sprites_id>
```

### Example

```
./build_sprites.py _ b f __ 0b 5f b____B ibc__F fffCCC
```

See [Sprite id syntax](#sprite-id-syntax) section below for more information about this notation.

## Installation

### Cairo

If you use png export, you first have to install pkg-config and cairo including its headers. On Ubuntu:

```
sudo apt install libcairo2-dev pkg-config python3-dev
```

See [Pycairo's getting started](https://pycairo.readthedocs.io/en/latest/getting_started.html) to get instructions on other systems.

### Python dependencies

Install Python dependencies on a new virtual environment:

```
python -m venv .venv
.venv/bin/pip install -r requirements.txt
```

## Sprite id syntax

The sprite id syntax is used to identify sprites, for instance to generate them via the CLI.

It allows users to define a position and a type for each slot.

### Slot position

Hexacode works with hexagon tiles, so each of the 6 edges can be used as a function input or output. In sprite id syntax, each edge is identified by a number:

- `0`: west
- `1`: north-west
- `2`: south-west
- `3`: north-east
- `4`: south-east
- `5`: east

### Slot type

The slot type informs, with one letter, the type of data that can be used on a slot. Since Hexacode is strongly typed, input slots can only receive data related to its type.

The special type `_` is used to describe an unused slot which can not be used as input or output.

The list of supported types is defined in `./sprites_config.yml`. Here are some examples:

- `b`: boolean
- `c`: char
- `i`: integer
- `f`: float

The letter case is used to inform if the slot is an input or an output. Therefore, `b` represent an input boolean, and `B` an output boolean.

### Sprite categories

There is three sprite categories, which depends on the slot id length.

Each of them have their syntax, described bellow. 

#### Constants

Used to create *constant* tiles.

- length: 1 char
- syntax: `<type>`
- examples: `_`, `b`, `f`

#### Funtions

Used to create *function* tiles.

- length: 6 chars
- syntax: `<type><type><type><type><type><type>`
- examples: `b____B`, `ibc__F`, `fffCCC`

The position of each type informs the slot direction.

#### Function parts

Used among other function parts in order to compose a full function tile. This is useful for programs that builds custom tiles.

- length: 2 chars
- syntax: `<direction><type>`
- examples: `__`, `0b`, `5f`

The special syntax `__` is used to identify an empty sprite (aka "star") on which function parts can be overlayed.
